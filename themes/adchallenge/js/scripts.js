(function ($) {
  var aboutImage = $(".img-right-image .img-about");
  aboutImage.attr("src", "/AD-Naloga/frontend-challenge/themes/adchallenge/images/music-about.png");

  var readmore = $(".news-date li.node-readmore a");

  readmore.append("<img src='/AD-Naloga/frontend-challenge/themes/adchallenge/images/arrow-blue.svg' class='arrow' alt='Arrow'>");

  readmore.hover(function() {
    $(this).children(".arrow").attr("src", "/AD-Naloga/frontend-challenge/themes/adchallenge/images/arrow-red.svg");
  })

  readmore.mouseleave(function() {
    $(this).children(".arrow").attr("src", "/AD-Naloga/frontend-challenge/themes/adchallenge/images/arrow-blue.svg");
  })


  var activeLink = $(".menu-main-item:first-child").addClass("active");

  var link = $(".menu-main-link");

  link.on("click", function (event) {
    event.preventDefault();
    var hash = this.hash;
    $("body, html").animate(
      {
        scrollTop: $(hash).offset().top - 99,
      },
      1000
    );
  });

  $(window).on("scroll",function () {
    var scrollBarLocation = $(this).scrollTop();

    if(scrollBarLocation > 99) {
      $("header").addClass("sticky");
    } else {
       $("header").removeClass("sticky");
    }

    link.each(function () {
      var sectionOffset = $(this.hash).offset().top - 100;

      if (sectionOffset <= scrollBarLocation) {
        $(this).parent().addClass("active");
        $(this).parent().siblings().removeClass("active");
      }
    });
  });

  var menu = $(".menu-main");
  var menuIcon = $(".menu-icon");

  if (window.matchMedia("(max-width: 900px)").matches) {
    menuIcon.on("click", function () {
      menu.toggleClass("show-mobile");
    });
  }

  $(window).on("resize", function () {
    if (window.matchMedia("(min-width: 901px)").matches) {
      if (menu.hasClass("show-mobile")) {
        menu.removeClass("show-mobile");
      }
    }
  });
})(jQuery);
